LOGLEVEL ?= NOTSET

VIRTUALENV_DIR := .venv

.PHONY: test
test: $(VIRTUALENV_DIR)
	LOGLEVEL=$(LOGLEVEL) poetry run python -m unittest

.PHONY: selenium
selenium:
	docker run --detach --publish=4444:4444 --shm-size=2g selenium/standalone-firefox

$(VIRTUALENV_DIR):
	poetry install
