# YouTube channel web feed bookmarklet

[![pipeline status](https://gitlab.com/victor-engmark/YouTube-channel-web-feed-bookmarklet/badges/master/pipeline.svg)](https://gitlab.com/victor-engmark/YouTube-channel-web-feed-bookmarklet/-/commits/master)

**Go to the web feed of the channel you’re currently on.** Currently works for user, channel and video pages.

YouTube currently returns Atom format feeds. I don't know whether they also support RSS.

## Install

1. Create a new bookmark called something like “YouTube channel web feed”.
1. Paste the contents of [bookmarklet.js](bookmarklet.js) into the "Location" field.
1. Save the bookmark.

## Use

1. Go to a YouTube page associated with a channel, such as the [Marble Machine](https://www.youtube.com/watch?v=IvUU8joBb1Q).
1. Click the bookmarklet to go to the web feed page.
1. Profit! I mean, now you have the URL of the feed and can easily copy it to somewhere like a feed reader.

## License

[AGPLv3+](LICENSE)
