#!/usr/bin/env bash

set -o errexit

cd "$(dirname "${BASH_SOURCE[0]}")"

poetry install

poetry run pre-commit install --hook-type=commit-msg --overwrite
poetry run pre-commit install --hook-type=pre-commit --overwrite
