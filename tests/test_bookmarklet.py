import logging
import warnings
from os import environ
from os.path import dirname, join
from unittest import TestCase

from selenium.webdriver import DesiredCapabilities, Remote
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

FEED_URL = (
    "https://www.youtube.com/feeds/videos.xml?channel_id=UCBR8-60-B28hp2BmDPdntcQ"
)

PROJECT_DIRECTORY = dirname(dirname(__file__))

logger = logging.getLogger(__name__)
log_handler = logging.StreamHandler()
log_level = environ.get("LOGLEVEL", logging.NOTSET)

logger.addHandler(log_handler)
logger.setLevel(log_level)


class VariableDefined:  # pylint: disable=too-few-public-methods
    def __init__(self, variable_name):
        self.variable_name = variable_name

    def __call__(self, driver):
        script = f"return typeof {self.variable_name} !== undefined;"
        return driver.execute_script(script)


class TestBookmarklet(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with open(join(PROJECT_DIRECTORY, "bookmarklet.js")) as file_pointer:
            cls.bookmarklet_code = file_pointer.read()
        logger.debug("Bookmarklet code: %s", cls.bookmarklet_code)

        warnings.filterwarnings(
            action="ignore", message="unclosed", category=ResourceWarning
        )
        selenium_host = environ.get("SELENIUM_HOST", "127.0.0.1")
        logger.debug("Selenium host: %s", selenium_host)
        cls.driver = Remote(
            command_executor=f"http://{selenium_host}:4444/wd/hub",
            desired_capabilities=DesiredCapabilities.FIREFOX.copy(),
        )

    def tearDown(self) -> None:
        if not self.successful():
            self.driver.save_screenshot(f"{self.id()}.png")

    def successful(self) -> bool:
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)

        for non_success_list in result.errors + result.failures:
            if non_success_list and non_success_list[-1][0] is self:
                return False

        return True

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()

    def test_should_go_to_feed_page_from_video_page(self) -> None:
        video_url = "https://www.youtube.com/watch?v=rdwz7QiG0lk"
        self.driver.get(video_url)
        WebDriverWait(self.driver, 10).until(VariableDefined("ytInitialPlayerResponse"))

        self.driver.execute_script(self.bookmarklet_code)
        WebDriverWait(self.driver, 10).until(expected_conditions.url_changes(video_url))

        self.assertEqual(self.driver.current_url, FEED_URL)

    def test_should_go_to_feed_page_from_channel_page(self) -> None:
        channel_url = "https://www.youtube.com/channel/UCBR8-60-B28hp2BmDPdntcQ"
        self.driver.get(channel_url)

        self.driver.execute_script(self.bookmarklet_code)
        WebDriverWait(self.driver, 10).until(
            expected_conditions.url_changes(channel_url)
        )

        self.assertEqual(self.driver.current_url, FEED_URL)

    def test_should_go_to_feed_page_from_user_page(self) -> None:
        channel_url = "https://www.youtube.com/user/YouTube"
        self.driver.get(channel_url)

        self.driver.execute_script(self.bookmarklet_code)
        WebDriverWait(self.driver, 10).until(
            expected_conditions.url_changes(channel_url)
        )

        self.assertEqual(self.driver.current_url, FEED_URL)
